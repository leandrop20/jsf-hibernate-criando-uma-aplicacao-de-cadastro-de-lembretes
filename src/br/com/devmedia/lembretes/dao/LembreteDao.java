package br.com.devmedia.lembretes.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.devmedia.lembretes.bean.JpaResourceBean;
import br.com.devmedia.lembretes.model.Lembrete;

public class LembreteDao {

	public List<Lembrete> listarTodos() throws Exception {
		EntityManager em = JpaResourceBean.getEm();
		List<Lembrete> lembretes = null;
		
		try {
			lembretes = em.createQuery("from Lembrete", Lembrete.class).getResultList();
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			em.close();
		}
		return lembretes;
	}
	
	public Lembrete selecionar(long id) throws Exception {
		EntityManager em = JpaResourceBean.getEm();
		Lembrete lembrete = null;
		
		try {
			lembrete = em.find(Lembrete.class, id);
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			em.close();
		}
		return lembrete;
	}
	
	public void inserir(Lembrete lembrete) throws Exception {
		EntityManager em = JpaResourceBean.getEm();
		
		try {
			em.getTransaction().begin();
			em.persist(lembrete);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			
			throw new Exception(e);
		} finally {
			em.close();
		}
	}
	
	public void atualizar(Lembrete lembrete) throws Exception {
		EntityManager em = JpaResourceBean.getEm();
		
		try {
			em.getTransaction().begin();
			em.merge(lembrete);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			
			throw new Exception(e);
		} finally {
			em.close();
		}
	}
	
	public void excluir(long id) throws Exception {
		EntityManager em = JpaResourceBean.getEm();
		
		try {
			em.getTransaction().begin();
			Lembrete lembrete = em.find(Lembrete.class, id);
			em.remove(lembrete);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			
			throw new Exception(e);
		} finally {
			em.close();
		}
	}
	
}