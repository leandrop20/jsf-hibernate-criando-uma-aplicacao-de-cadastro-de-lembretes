package br.com.devmedia.lembretes.bean;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaResourceBean {

	private static EntityManagerFactory emf;
	
	public static EntityManager getEm() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("lembretes");
		}
		return emf.createEntityManager();
	}
	
}