package br.com.devmedia.lembretes.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.devmedia.lembretes.dao.LembreteDao;
import br.com.devmedia.lembretes.model.Lembrete;
import br.com.devmedia.lembretes.model.Prioridade;

@ManagedBean
@ViewScoped
public class LembreteBean {

	private Lembrete lembrete;
	private LembreteDao lembreteDao;
	private List<Lembrete> lembretes;
	
	@PostConstruct
	public void init() {
		lembreteDao = new LembreteDao();
		lembrete = new Lembrete();
		
		try {
			lembretes = lembreteDao.listarTodos();
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
					e.getMessage()));
			context.getExternalContext().getFlash().setKeepMessages(true);
		}
	}
	
	public Lembrete getLembrete() { return lembrete; }
	
	public List<Lembrete> getLembretes() { return lembretes; }
	
	public Prioridade[] getPrioridades() { return Prioridade.values(); }
	
	public void selecionar() {
		try {
			lembrete = lembreteDao.selecionar(lembrete.getId());
			
			if (lembrete == null || lembrete.getId() == 0) {
				lembrete = new Lembrete();
				
				throw new Exception("Lembrete n�o encontrado!");
			}
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getMessage());
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}
	
	public String inserir() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			lembreteDao.inserir(lembrete);
			lembrete = new Lembrete();
			
			lembretes = lembreteDao.listarTodos();
			
			context.addMessage(null, new FacesMessage("Lembrete adicionado com sucesso!"));
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), 
					e.getMessage()));
		}
		
		context.getExternalContext().getFlash().setKeepMessages(true);
		
		return "home";
	}
	
	public String atualizar() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			lembreteDao.atualizar(lembrete);
			lembrete = new Lembrete();
			
			lembretes = lembreteDao.listarTodos();
			
			context.addMessage(null, new FacesMessage("Lembrete editado com sucesso!"));
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
					e.getMessage()));
		}
		context.getExternalContext().getFlash().setKeepMessages(true);
		
		return "home";
	}

	public String excluir() {
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			lembreteDao.excluir(lembrete.getId());
			
			lembretes = lembreteDao.listarTodos();
			
			context.addMessage(null, new FacesMessage("Lembrete removido com sucesso!"));
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), 
					e.getMessage()));
		}
		
		context.getExternalContext().getFlash().setKeepMessages(true);
		
		return "home";
	}
	
}